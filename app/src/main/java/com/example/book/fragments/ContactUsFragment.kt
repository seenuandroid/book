package com.example.book.fragments


import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.book.R


/**
 * A simple [Fragment] subclass.
 */
class ContactUsFragment : Fragment() {
    private var mView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_contact_us, container, false)
        init()
        return mView
    }

    private fun init() {
        val email = mView?.findViewById<TextView>(R.id.tvEmail)
        val phone = mView?.findViewById<TextView>(R.id.tvCall)
        email?.setOnClickListener {
            sendEmail()
        }

        phone?.setOnClickListener {
            if (checkPermission()) {
                call()
            } else {
                requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 201)
            }
        }
    }

    private fun sendEmail() {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:csuzette26@aol.com"))
            intent.putExtra(Intent.EXTRA_SUBJECT, "")
            intent.putExtra(Intent.EXTRA_TEXT, "")
            startActivityForResult(intent, 100)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(requireContext(), "Gmail app not found", Toast.LENGTH_SHORT).show()
        }
    }

    private fun call() {
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:2145315492")
        requireContext().startActivity(intent)
    }

    private fun checkPermission() = ActivityCompat.checkSelfPermission(
        requireContext(),
        android.Manifest.permission.CALL_PHONE
    ) == PackageManager.PERMISSION_GRANTED


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (checkPermission()) {
            call()
        } else {
            Toast.makeText(requireContext(), "This requires Call permission, you can provide them from settings", Toast.LENGTH_SHORT).show();
        }
    }

}
