package com.example.book.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.book.R
import com.github.barteksc.pdfviewer.PDFView

class PDFFragment : Fragment() {
    private var mView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_pdf, container, false)
        loadPDF()
        return mView
    }

    private fun loadPDF() {
        val stram = requireContext().assets?.open("cleaning.pdf")
        val pdfView = mView?.findViewById<PDFView>(R.id.pdfView)
        pdfView?.fromStream(stram)?.load()
    }
}
