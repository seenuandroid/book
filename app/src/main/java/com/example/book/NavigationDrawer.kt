package com.example.book

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.book.fragments.ContactUsFragment
import com.example.book.fragments.PDFFragment
import com.google.android.material.navigation.NavigationView


class NavigationDrawer : AppCompatActivity() {

    private var dl: DrawerLayout? = null
    private var t: ActionBarDrawerToggle? = null
    private var nv: NavigationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_drawer)
        init()
    }

    private fun init() {
        dl = findViewById<DrawerLayout>(R.id.activity_main)
        t = ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close)
        dl?.addDrawerListener(t!!)
        t?.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        nv = findViewById<NavigationView>(R.id.nv)

        nv?.setNavigationItemSelectedListener { item ->
            dl?.closeDrawers()
            when (item.itemId) {
                R.id.book ->
                    replaceFragment(PDFFragment())
                R.id.contactUs ->
                    replaceFragment(ContactUsFragment())
            }
            true
        }
        replaceFragment(PDFFragment())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (t?.onOptionsItemSelected(item) != null) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun replaceFragment(destFragment: Fragment) {
        // First get FragmentManager object.
        val fragmentManager = this.supportFragmentManager

        // Begin Fragment transaction.
        val fragmentTransaction = fragmentManager.beginTransaction()

        // Replace the layout holder with the required Fragment object.
        fragmentTransaction.replace(R.id.containar, destFragment)

        // Commit the Fragment replace action.
        fragmentTransaction.commit()
    }
}
